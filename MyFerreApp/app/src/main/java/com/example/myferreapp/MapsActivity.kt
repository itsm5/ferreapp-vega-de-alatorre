package com.example.myferreapp

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Camera
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.core.content.ContextCompat
import com.example.myferreapp.databinding.ActivityMapsBinding

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.canvas.CanvasCompat
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private val ferreterias = mutableListOf<Ferreteria>()
    private lateinit var myLocationButton: FloatingActionButton
    private lateinit var binding: ActivityMapsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        addFerreteria()
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun addFerreteria() {
        ferreterias.add(Ferreteria("Ferretería El Puro",20.026643,-96.6534804))
        ferreterias.add(Ferreteria("Ferretería y materiales Tajín",20.0290566,-96.6436853))
        ferreterias.add(Ferreteria("Ferretería La Ferre", 20.0303607,-96.6473906))
        ferreterias.add(Ferreteria("Ferretería y materiales Krystel", 20.0303559,-96.6460774))
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val icon = getFerreIcon()
        for( ferreteria in ferreterias){
            val ferreteriaPosition = LatLng(ferreteria.latitud, ferreteria.longitud)
            val ferreteriaName = ferreteria.name
            val markerOptions = MarkerOptions().position(ferreteriaPosition).title(ferreteriaName)
                .icon(icon)
            mMap.addMarker(markerOptions)
        }

        // Add a marker in Sydney and move the camera
        val Ferre1 = LatLng(20.0313061,-96.6431574 )
        mMap.addMarker(MarkerOptions().position(Ferre1).title("Ferretería Sumano")).setIcon(icon)
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Ferre1))

        binding.myLocation.setOnClickListener{
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Ferre1,16.0f))
        }
    }

    private fun getFerreIcon(): BitmapDescriptor {
        val drawable = ContextCompat.getDrawable(this, R.drawable.ic_martillo)
        drawable?.setBounds(0,0,drawable.intrinsicWidth,drawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap( drawable?.intrinsicWidth ?: 0, drawable?.intrinsicHeight ?: 0, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable?.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }
}


